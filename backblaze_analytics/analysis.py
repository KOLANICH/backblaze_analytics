from pathlib import Path
from lazily import lazyImport
from lazily import scipy as np
from lazily import pandas
plt = lazyImport("matplotlib.pyplot")

from . import database
from .dataset import Dataset
from .utils.mtqdm import mtqdm


from .utils.PickleCache import PickleCache

#from .datasetDescription import attrsSpec
#from Chassis import Chassis


def genDomainsCustomGeneratorMapping(attrName):
	def func(ds, pds):
		ar = getattr(ds, attrName)
		return range(ar.base, len(ar) + 1)

	return (attrName[:-1] + "_id", func)  # models -> model_id


specialDomainsMapping = dict((genDomainsCustomGeneratorMapping(attrName) for attrName in Dataset.indexes))


class Analysis:
	"""A class to make analysis. Call its methods in a Jupyter notebook"""

	def makePds(self):
		availableKeys = self.ds.getAvailableAttrs()
		return self.createDataFrame(availableKeys)

	def loadAndAugmentDataset(self):
		print("Using the DB " + str(self.dbPath) + "...")
		print("Getting index....")
		ds = Dataset(self.dbPath)
		print("Augmenting...")
		ds.augment()
		return ds

	CACHE_NAMESPACE = "analysis"

	def __init__(self, dbFilePath):
		self.dbPath = dbFilePath
		self.pch = PickleCache({
			"pds": self.makePds,
			"ds": self.loadAndAugmentDataset
		}, self.__class__.CACHE_NAMESPACE)
		self.domains = {}

	@property
	def ds(self):
		return self.pch.ds

	@property
	def pds(self):
		return self.pch.pds

	#@property
	#def domains(self):
	#	return self.pch.domains

	def getSetOfValues(self, modelAttr):
		"""Gives a set of values of the attribute with passed name. Use it with categorial attributes."""
		res = set(filter(lambda x: x is not None, {m[modelAttr] for m in self.ds.models if modelAttr in m}))
		try:
			return set(sorted(res))
		except:
			return res

	def insertModelAttrIntoPandasDataset(self, pds, attrName):
		"""Walks the pandas dataframe with dataset and adds attributes for drives. The source of attributes is a description of a model of a drive."""
		if attrName not in self.domains:
			self.computeDomains(attrName)

		def getAttrForModelId(x):
			if attrName in self.ds.models[x]:
				return self.ds.models[x][attrName]
			else:
				return None
		pds.loc[:, attrName] = pds.loc[:, "model_id"].apply(getAttrForModelId)

	def computeDomains(self, *attrs):
		for attrName in attrs:
			if attrName in specialDomainsMapping:
				self.domains[attrName] = specialDomainsMapping[attrName](self.ds, self.pds)
			else:
				self.domains[attrName] = self.getSetOfValues(attrName)
			#print("domains", attrName, self.domains[attrName])

	def createDataFrame(self, additionalAttrs):
		"""Initializes PandasDataFrame with the data from dataset and does some other addituonal operations."""
		self.computeDomains(*additionalAttrs)

		print("The database contains " + ("reduced" if self.ds.reduced else "full") + " dataset")
		print("Getting stats from dataset....")
		statz = Dataset.stats(self.dbPath, reduced=self.ds.reduced)

		print("Creating a dataframe from stats....")
		pds = pandas.DataFrame.from_dict(statz)
		del (statz)

		pds["model_id"] = pds.loc[:, "id"].apply(lambda id: self.ds.drives[id]["model_id"])
		pds["brand_id"] = pds.loc[:, "model_id"].apply(lambda id: self.ds.models[id]["brand_id"])
		pds["vendor_id"] = pds.loc[:, "brand_id"].apply(lambda id: self.ds.brands[id]["vendor_id"])
		#print(pds["vendor_id"])

		for attrName in additionalAttrs:
			self.insertModelAttrIntoPandasDataset(pds, attrName)

		if not self.ds.reduced:
			pds["failed"] = pds.loc[:, "failure_worked_days_smart"].notnull()
			pds["duration_worked"] = pds.loc[:, "failure_worked_days_smart"].where(pds.loc[:, "failed"], pds.loc[:, "total_worked_days_smart"])
		else:
			pds["failed"] = pds.loc[:, "days_in_dataset_failure"].notnull()
			pds["duration_worked"] = pds.loc[:, "days_in_dataset_failure"].where(pds.loc[:, "failed"], pds.loc[:, "days_in_dataset"])
		pds.sort_values(by="duration_worked", inplace=True)
		return pds
