import sys
import typing
from os import isatty
from operator import itemgetter, attrgetter, methodcaller
from pathlib import Path
from types import FunctionType
from gc import collect
import re
import warnings

from lazily import lazyImport
from lazily import numpy as np
from lazily import pandas
from lazily import lifelines
import lazily.lifelines.utils

from .. import database
from ..analysis import Analysis

from ..utils.mtqdm import mtqdm

from datetime import timedelta
from ..utils import fancyTimeDelta

from plumbum import cli
from .DatabaseCommand import DatabaseCommand
from ..utils.PickleCache import PickleCache
from ..datasetDescription import spec

import math

from NoSuspend import NoSuspend

from ..utils.XGBoostCoxPHFitter import XGBoostCoxPHFitter
from .. import augment

#from Chassis import Chassis

import pandas._libs.json as json # automatically handles nans, infs and other shit

def explainItemUsingSHAP(shapValues:"pandas.DataFrame", thresholdRatio=50.):
	res = []
	for idx, el in shapValues.iterrows():
		normConst = el.sum()
		el /= normConst
		shapValsSignificance = el.abs().sort_values(ascending=False)
		minThresh = shapValsSignificance[0] / thresholdRatio
		selector = shapValsSignificance>minThresh
		shapValsSignificance = shapValsSignificance[selector]
		significantShaps = el[shapValsSignificance.index]
		resDict = significantShaps.to_dict()
		resDict["$other"] = el[~selector].sum()
		res.append(resDict)
	return res

class CoxRegressionAnalysis(Analysis):
	"""A class to make analysis. Call its methods in a Jupyter notebook"""

	def smartSample(self, frac: float):
		"""XGBoost may fail if all the records are used, presumably because of memory. Here we try to sample the rows in the way keep the most informative samples:
			* the one that have failed.
			* ~~the one having the longest life~~ (No, this way we can miss the info about medium-lived drives)
		"""
		assert frac <= 1. and frac > 0.

		if frac == 1.:
			print("frac=1., keeping all the drives")
			return self.pds

		failedSelector = self.pds.loc[:, "failed"]
		pdsFailed = self.pds.loc[failedSelector]
		pdsAlive = self.pds.loc[~failedSelector]

		totalCount = len(self.pds)
		drivesToKeep = int(np.floor(totalCount * frac))
		failedCount = len(pdsFailed)
		aliveDrivesToKeep = drivesToKeep - failedCount

		if aliveDrivesToKeep > 0:
			print("All the failed drives (", failedCount / totalCount, " of the dataset) fit into the frac, using", aliveDrivesToKeep / len(pdsAlive), "of censored drives")
			keptAliveDrives = pdsAlive.sample(n=aliveDrivesToKeep)
			return pandas.concat([pdsFailed, keptAliveDrives], axis=0)
		else:
			print("Not all the failed drives (", failedCount / totalCount, " of the dataset) fit into the frac, using", (aliveDrivesToKeep - failedCount) / failedCount, " of failed drives")
			return pdsFailed.sample(n=failedCount + aliveDrivesToKeep, axis=0)

	@property
	def pds(self):
		if self.pch:
			return super().pds
		else:
			return self._pds

	def __init__(self, dbFilePath: Path, frac: float = 0.7, prefix="./Cox_XGBoost_Models"):
		super().__init__(dbFilePath)
		self._pds = self.smartSample(frac)
		self.spec=None
		self.engineerFeatures(self._pds)
		
		self.pch = None
		collect()
		self.f = XGBoostCoxPHFitter(self.spec, prefix=prefix)

	def engineerFeatures(self, pds):
		n = "numerical"
		if	self.spec is None:
			self.spec=type(spec)(spec)
			#self.spec["brand"] = "categorical"
			#self.spec["vendor"] = "categorical"
			#self.spec["form_factor_crossection_side"] = n
			#self.spec["form_factor_crossection_front"] = n
			#self.spec["form_factor_volume"] = n
			#self.spec["form_factor_crossection_top"] = n
			#self.spec["platter_linear_speed"] = n
			#self.spec["platter_separation"] = n
		
		#pds.loc[:, "platter_linear_speed"] = pds.loc[:, ["form_factor_width", "form_factor_depth"]].min(axis=1)/2*pds.loc[:, "rpm"]**2
		#pds.loc[:, "platter_separation"] = pds.loc[:, "form_factor_height"]/pds.loc[:, "platters"]**2
		collect()
		#pds.loc[:, "form_factor_volume"] = pds.loc[:, ["form_factor_width", "form_factor_height", "form_factor_depth"]].product(1)
		#pds.loc[:, "form_factor_crossection_front"] = pds.loc[:, ["form_factor_width", "form_factor_height"]].product(1)
		#pds.loc[:, "form_factor_crossection_side"] = pds.loc[:, ["form_factor_height", "form_factor_depth"]].product(1)
		#pds.loc[:, "form_factor_crossection_top"] = pds.loc[:, ["form_factor_width", "form_factor_depth"]].product(1)
		
		#pds.loc[:, "interface"] = pds.loc[:, "interface"].apply(lambda e: e if e != "600" else "SATA")
		#pds.loc[:, "brand"] = pds.loc[:, "brand_id"].map(lambda id: self.ds.brands[id]["name"])
		#pds.loc[pds.loc[:, "variable_rpm"] == True, "variable_rpm"] = 1.0
		#pds.loc[pds.loc[:, "variable_rpm"] == False, "variable_rpm"] = 0.0
		#pds.loc[pds.loc[:, "variable_rpm"].isnull(), "variable_rpm"] = math.nan

	def getDefaultHyperparams(self):
		return {
			"colsample_bytree": 0.852646728688084,
			"learning_rate": 0.44999999970000004,
			"max_depth": 5,
			"min_child_weight": 0.00036131818682314773,
			"min_split_loss": 1.7193612317506807e-05,
			"reg_alpha": 0.002685834045538217,
			"subsample": 0.99999999881,
			"num_boost_round": 34
		}

	def selfTest(self, predicted=None):
		print("Testing that XGBoost is not broken on this count of rows...")
		if predicted is None:
			self.f.hyperparams = self.getDefaultHyperparams()
			self.f.fit(self.pds, "duration_worked", "failed", saveLoadModel=None)
			predicted = self.f.predict_log_partial_hazard(self.pds)

		countOfNans = predicted.isna().sum()[0]
		if countOfNans:
			raise Exception("Fucking shit. For this fraction of rows XGBoost fails. The result contains " + str(countOfNans) + " nans")

		print("Self-test has passed successfully")

	def optimizeHyperparams(self, iters=10000, optimizer=None, pointsStorageFileName: Path = "./UniOptPointsBackup.sqlite", finalCvFolds=2):
		from UniOpt.core.PointsStorage import SQLiteStorage

		pointsStorage = SQLiteStorage(pointsStorageFileName)
		with NoSuspend():
			self.selfTest()
			self.f.optimizeHyperparams(self._pds, "duration_worked", "failed", iters=iters, optimizer=None, pointsStorage=pointsStorage)
			print("Concordance via cv: ", self.evaluateModel(self._pds, finalCvFolds))
	
	def evaluateModel(self, folds:int=2):
		d = lifelines.utils.k_fold_cross_validation(self.f, self._pds, duration_col="duration_worked", event_col="failed", k=folds)
		return (np.mean(d), np.std(d))

	def trainModel(self, format="binary"):
		with NoSuspend():
			self.f.fit(self.pds, "duration_worked", "failed", saveLoadModel=False, format=format)
			predicted = self.f.predict_log_partial_hazard(self.pds)
			self.selfTest(predicted)
	
	def loadModel(self, format="binary"):
		self.f.fit(self.pds, "duration_worked", "failed", saveLoadModel=True, format=format)
	
	def preparePredictionOfUnknownModelsSurvival(self, models):
		models = augment.augment(models)
		msdf = pandas.DataFrame.from_records(models)
		self.engineerFeatures(msdf)
		return msdf

	def predictUnknownModelsSurvival(self, models, explain:float=10.):
		msdf = self.preparePredictionOfUnknownModelsSurvival(models)
		msdf.loc[:, "predicted_survival"] = self.f.predict_expectation(msdf, SHAPInteractions=(False if explain else None))[0]
		if explain:
			explainations = explainItemUsingSHAP(self.f.explainations[0])
		return msdf.sort_values("predicted_survival", ascending=False), explainations
	



# class CoxAnalysisCLI(DatabaseCommand, ImageOutputCommand): # cli.switches.SwitchError: Switch db-path already defined and is not overridable
class CoxAnalysisCLI(cli.Application):
	"""Tools for Cox regression analysis - the main purpose of this project"""
	pass


@CoxAnalysisCLI.subcommand("optimize-hyperparams")
class CoxOptimizeHyperparams(cli.Application):
	"""Fits hyperparams for XGBoost Cox regression using UniOpt"""

	reduced = cli.Flag(("r", "reduced"), help="do not use the info from S.M.A.R.T.. The numbers of days may be LESS ACCURATE since the time before appearing in the dataset is not counted (but it's available in S.M.A.R.T.).", default=True)
	dbPath = cli.SwitchAttr("--db-path", cli.ExistingFile, default=None, help="Path to the SQLite database")
	dsFrac = cli.SwitchAttr("--ds-frac", float, default=0.45, help="Fraction of the dataset used to train the XGBoost model. XGBoost may eat all the memory in the system and crash, if all the dataset is used, if this happens, reduce the fraction.")
	modelsPrefix = cli.SwitchAttr("--prefix", str, default="./Cox_XGBoost_Models", help="The directory where fitted XGBoost models and their metadata would reside.")

	optimizer = cli.SwitchAttr("--optimizer", float, default="MSRSM", help="select a UniOpt-supported hyperparams optimizer.")

	def main(self, *attrs):
		if self.dbPath is None:
			self.dbPath = "./drives.sqlite" if self.reduced else database.databaseDefaultFileName

		a = CoxRegressionAnalysis(self.dbPath, self.dsFrac, prefix=self.modelsPrefix)
		a.optimizeHyperparams(optimizer=self.optimizer)

@CoxAnalysisCLI.subcommand("evaluate")
class CoxEvaluate(cli.Application):
	"""Fits hyperparams for XGBoost Cox regression using UniOpt"""

	reduced = cli.Flag(("r", "reduced"), help="do not use the info from S.M.A.R.T.. The numbers of days may be LESS ACCURATE since the time before appearing in the dataset is not counted (but it's available in S.M.A.R.T.).", default=True)
	dbPath = cli.SwitchAttr("--db-path", cli.ExistingFile, default=None, help="Path to the SQLite database")
	dsFrac = cli.SwitchAttr("--ds-frac", float, default=0.53, help="Fraction of the dataset used to train the XGBoost model. XGBoost may eat all the memory in the system and crash, if all the dataset is used, if this happens, reduce the fraction.")
	modelsPrefix = cli.SwitchAttr("--prefix", str, default="./Cox_XGBoost_Models", help="The directory where fitted XGBoost models and their metadata would reside.")
	folds = cli.SwitchAttr("--folds", int, default=4, help="Count of folds in concordance crossvlidation")
	
	def main(self, *attrs):
		if self.dbPath is None:
			self.dbPath = "./drives.sqlite" if self.reduced else database.databaseDefaultFileName

		a = CoxRegressionAnalysis(self.dbPath, self.dsFrac, prefix=self.modelsPrefix)
		print(a.evaluateModel(folds=self.folds))

@CoxAnalysisCLI.subcommand("fit")
class CoxTrainModel(cli.Application):
	"""Fits and saves XGBoost Cox model. Need to do it in order to use the model."""

	reduced = cli.Flag(("r", "reduced"), help="do not use the info from S.M.A.R.T.. The numbers of days may be LESS ACCURATE since the time before appearing in the dataset is not counted (but it's available in S.M.A.R.T.).", default=True)
	dbPath = cli.SwitchAttr("--db-path", cli.ExistingFile, default=None, help="Path to the SQLite database")
	dsFrac = cli.SwitchAttr("--ds-frac", float, default=0.7, help="Fraction of the dataset used to train the XGBoost model. XGBoost may break when all the dataset is used (in this case the resulting model returns `nan`), if this happens, reduce the fraction.")

	modelFormat = cli.SwitchAttr("--format", str, default="binary", help="which format of XGBoost models to use")  # currently pyxgboost doesn't support survival:cox
	#modelFormat="binary"

	def main(self, *attrs):
		if self.dbPath is None:
			self.dbPath = "./drives.sqlite" if self.reduced else database.databaseDefaultFileName
		a = CoxRegressionAnalysis(self.dbPath, self.dsFrac)
		a.trainModel(format=self.modelFormat)



jsonProbeRx=re.compile("^\\s*\\[\\s*\\{\\s*\"?")
def preprocessModelDescriptors(models):
	if isinstance(models, str):
		try:
			models = json.loads(models)
			if isinstance(models, dict):
				models = [models]
		except Exception:
			def dumbText():
				nonlocal models
				models = str.splitlines(models)
			
			if jsonProbeRx.match(models):
				try:
					import json5
					models = json5.loads(models)
					warnings.warn("Incorrect JSON is used. Parsed with a DAMN SLOW json5 lib. Fix the fucking shit!")
				except ImportError as ex:
					raise Exception("Incorrect string. Begins like JSON, but it is not. Unable to test if it is JSON5 - the lib is not present.") from ex
					
			else:
				dumbText()
	
	ms = []
	for m in models:
		if isinstance(m, str):
			try:
				m = json.loads(m)
			except:
				m = {"name": m}
			
		ms.append(m)
	return ms


wsRx=re.compile("\s")
def splitByWhitespaces(file):
	for l in file:
		yield from wsRx.split(l)


def pred2obj(res, explainations, err=None):
	resp = {}
	if res is not None:
		resp["res"]=res.to_dict(orient="records")
	if explainations is not None:
		resp["explainations"] = explainations
	if err is not None:
		resp["error"] = str(err)
	return resp

class info2formatsMeta(type):
	def __new__(cls, className: str, parents, attrs: typing.Dict[str, typing.Any], *args, **kwargs):
			newAttrs = type(attrs)(attrs)
			#try:
			from pytablewriter import MarkdownTableWriter, JavaScriptTableWriter, PythonCodeTableWriter, RstGridTableWriter, MediaWikiTableWriter, TomlTableWriter
			from functools import partial
			pytablewriterFormats={
				"markdown": (MarkdownTableWriter, "text/markdown", "md"),
				"rst": (RstGridTableWriter, "text/x-rst", "rst"),
				"mediawiki": (MediaWikiTableWriter, "text/plain", "mediawiki"),
				"js": (JavaScriptTableWriter, "application/javascript", "js"),
				"py": (PythonCodeTableWriter, "application/x-python-code", "py"),
				"toml": (TomlTableWriter, "application/x-toml", "toml")
			}
			def pytablewriterFormatWriter(writerType, mime, ext, res, explainations, err=None):
				if res is not None:
					w = writerType()
					try:
						w.table_name = "Regression data"
					except:
						pass
					w.from_dataframe(res)
					return w.dumps(), mime, ext
				else:
					return str(err), "text/plain", "txt"
			for k, descriptor in pytablewriterFormats.items():
				newAttrs[k] = partial(pytablewriterFormatWriter, *descriptor)
			#except:
			pass
		
			newAttrs["_availableFormats"] = set((a for a in newAttrs.keys() if a[0]!="_"))
			return super().__new__(cls, className, parents, newAttrs, *args, **kwargs)

class info2formats(metaclass=info2formatsMeta):
	def json(res, explainations, err=None):
		return json.dumps(pred2obj(res, explainations, err)), "application/json", "json"
	
	def html(res, explainations, err=None):
		if res is not None:
			# TODO: ❌ and ✅ for boolean columns
			# TODO: importances highlighting
			return res.to_html(), "text/html", "html"
		else:
			return str(err), "text/html", "html"
	
	def latex(res, explainations, err=None):
		if res is not None:
			return res.to_latex(), "application/x-latex", "tex"
		else:
			return str(err), "text/plain", "txt"
	
	def tsv(res, explainations, err=None):
		if res is not None:
			return res.to_csv(sep='\t'), "text/tab-separated-values", "tsv"
		else:
			return str(err), "text/plain", "txt"
	
	def csv(res, explainations, err=None):
		if res is not None:
			return res.to_csv(), "text/csv", "csv"
		else:
			return str(err), "text/plain", "txt"
	

class CoxPredictModel(cli.Application):
	"""Fits and saves XGBoost Cox model. Need to do it in order to use the model."""

	reduced = cli.Flag(("r", "reduced"), help="do not use the info from S.M.A.R.T.. The numbers of days may be LESS ACCURATE since the time before appearing in the dataset is not counted (but it's available in S.M.A.R.T.).", default=True)
	dbPath = cli.SwitchAttr("--db-path", cli.ExistingFile, default=None, help="Path to the SQLite database")
	dsFrac = cli.SwitchAttr("--ds-frac", float, default=0.7, help="Fraction of the dataset used to train the XGBoost model. XGBoost may break when all the dataset is used (in this case the resulting model returns `nan`), if this happens, reduce the fraction.")
	format = cli.SwitchAttr("--format", cli.Set(*info2formats._availableFormats, case_sensitive = True), default="json", help="(Default) format to use")
	
	def prepare(self):
		if self.dbPath is None:
			self.dbPath = "./drives.sqlite" if self.reduced else database.databaseDefaultFileName
		
		pandas.set_option('display.max_columns', None)
		self.analysis = CoxRegressionAnalysis(self.dbPath, self.dsFrac)
		self.analysis.loadModel()

	def computeAndRenderResults(self, modelDescriptors, format=None):
		if format is None:
			format = self.format
		
		if format in info2formats._availableFormats:
			try:
				modelDescriptors = preprocessModelDescriptors(modelDescriptors)
				res, explainations = self.analysis.predictUnknownModelsSurvival(modelDescriptors)
				def fancifyTime(s):
					if s and not math.isnan(s):
						ft = fancyTimeDelta(timedelta(days=s))
						return (str(ft.years) + " y, " if ft.years else "") + (str(ft.months) + " m, " if ft.months else "") + str(ft.days) + " d"
					else:
						return s
				
				res.loc[:, "$fancyTime"] = res.loc[:, "predicted_survival"].map(fancifyTime)
				ex = None
			except Exception as ex1:
				res, explainations = (None, None)
				ex = ex1
			return getattr(info2formats, format)(res, explainations, ex)
		else:
			return "Format `"+format+"` is not available, use one of "+str(availableFormats), "text/plain", "txt"
	

@CoxAnalysisCLI.subcommand("predict")
class CoxPredictModelCLI(CoxPredictModel):
	"""Predicts survival for the drives which descriptors (just namebers or JSON) are passed via CLI"""
	def main(self, *modelDescriptors):
		if not modelDescriptors:
			if not isatty(sys.stdin.fileno()):
				modelDescriptors = sys.stdin.read()
			else:
				self.help()
				return -1
		
		self.prepare()
		
		print(self.computeAndRenderResults(modelDescriptors)[0])

@CoxAnalysisCLI.subcommand("server")
class CoxPredictionServer(CoxPredictModel):
	allowExternalAccess = cli.Flag("--external-access", help="Allow accesses not from localhost.", default=False)
	startBrowser = cli.Flag(("-B", "--start-browser"), help="Starts a web browser automatically", default=False)
	CORS = cli.Flag(("-C", "--CORS"), help="Enables CORS", default=True)
	port = cli.SwitchAttr("--port", int, default=0xbb85, help="Port to start server on")
	
	def main(self, *args):
		self.prepare()
		
		from aiohttp import web, TCPConnector, ClientSession
		app = web.Application()
		
		routes = web.RouteTableDef()
		
		@routes.get('/')
		async def help(request):
			return web.Response(text="Send a POST to /predict.<br/><form action='predict' method='POST'><select name='format'>"+"".join(("<option>"+fId+"</option>") for fId in info2formats._availableFormats)+"</select><textarea name='models' width='100%' height='100%'></textarea><input type='submit'></form>", content_type="text/html")
		
		@routes.get('/status')
		async def status(request):
			f=self.analysis.f
			axg=f.axg
			return web.json_response({"columns": list(axg.columns), "bestHyperparams": axg.bestHyperparams})
		
		def predictForData(modelDescriptors, format):
			resContent, mimeType, ext = self.computeAndRenderResults(modelDescriptors, format)
			resp=web.Response(text=resContent, content_type=mimeType)
			resp.headers["Content-Disposition"]='inline; filename="prediction.'+ext+'"'
			return resp
		
		@routes.get('/predict/{models}')
		async def predictGet(request):
			return predictForData(request.match_info['models'], None)
		
		@routes.get('/predict/{models}/{format}')
		async def predictGetFormat(request):
			return predictForData(request.match_info['models'], request.match_info['format'])
		
		async def predictPost_(request, format):
			formData = await request.post()
			if formData:
				if formData["models"]:
					data=formData["models"]
				else:
					data=await request.text()
			
				if formData["format"]:
					format=formData["format"]
			else:
				data=await request.text()
			
			return predictForData(data, format)
		
		@routes.post('/predict')
		async def predictPost(request):
			return await predictPost_(request, None)
		
		@routes.post('/predict/{format}')
		async def predictPostFormat(request):
			return await predictPost_(request, request.match_info['models'])
		
		if self.CORS:
			import aiohttp_cors
			cors = aiohttp_cors.setup(app)
			predictPostResource = cors.add(app.router.add_resource('/predict'))
			route = cors.add(
				predictPostResource.add_route("POST", predictPost),
				{
					"*": aiohttp_cors.ResourceOptions(allow_headers="*")
				}
			)
		else:
			app.add_routes([web.post('/', predictPost)])
		
		@web.middleware
		async def logger(request, handler):
			peerName=request.transport.get_extra_info('peername')
			print("Connection from ", *peerName, "to", request.path)
			return await handler(request)
		app.middlewares.append(logger)
		
		if not self.allowExternalAccess:
			@web.middleware
			async def accessRestricter(request, handler):
				peerName=request.transport.get_extra_info('peername')
				remoteIP=peerName[0].split(".")
				if (len(remoteIP) != 4 and remoteIP[0] != 127 and remoteIP[1] != 0 and remoteIP[2] != 0):
					raise web.HTTPForbidden("Fuck off!")
				
				return await handler(request)
			app.middlewares.append(accessRestricter)
		async def initApp(app):
			pass
			# Don't use aiohttp_remotes, it trusts a remote user blindly
		
		#resp.headers['Server'] = 'BackBlaze_Analytics_Cox_Regression'
		
		app.on_startup.append(initApp)
		app.add_routes(routes)
		web.run_app(app, port=self.port)
		
		serverURI = "http://localhost:"+self.port
		print(serverURI)
		if self.startBrowser:
			import webbrowser
			try:
				webbrowser.open(serverURI)
			except:
				print("Cannot start browser")


if __name__ == "__main__":
	CoxAnalysisCLI.run()
