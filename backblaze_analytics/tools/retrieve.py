import sys, re, os, shlex
from lazily import lazyImport
from .CommandsGenerator import commandGen
import lazy_object_proxy
from lazily import bs4
from datetime import datetime, timedelta, timezone

database = lazyImport("..database")


def makeRequestsSession():
	import requests

	reqSess = requests.Session()
	try:
		import hyper
		from hyper.contrib import HTTP20Adapter

		ad = HTTP20Adapter()
		reqSess.mount("https://", ad)
		reqSess.mount("http://", ad)
	except:
		pass
	return reqSess


reqSess = lazy_object_proxy.Proxy(makeRequestsSession)

dsListUri = "https://www.backblaze.com/b2/hard-drive-test-data.html"

yearRx = re.compile("20[12]\d")
typeRx = re.compile("data|docs")
quartalRx = re.compile("Q([1-4])")

quartalMonthLength = 3
yearMonthLength = 12


def decodeTimespan(year, quartal=None):
	lowerBoundMonth = 1
	lowerMonthDelta = 0
	if quartal:
		lowerMonthDelta = quartalMonthLength * (quartal - 1)
		higherMonthDelta = lowerMonthDelta + quartalMonthLength
	else:
		higherMonthDelta = 12

	yearDelta = higherMonthDelta // yearMonthLength
	higherMonthDelta %= yearMonthLength

	return (
		datetime(year=year, month=lowerBoundMonth + lowerMonthDelta, day=1, tzinfo=timezone.utc),
		datetime(year=year + yearDelta, month=lowerBoundMonth + higherMonthDelta, day=1, tzinfo=timezone.utc)
	)


class BackblazeDatasetDownload:
	"""A piece of Backblaze dataset available for download"""

	def __init__(self, uri):
		name = os.path.basename(uri)
		year = int(yearRx.search(name).group(0))
		type = typeRx.search(name).group(0)
		qM = quartalRx.search(name)
		if qM:
			quartal = int(qM.group(1))
		else:
			quartal = None
		self.name = name
		self.year = year
		self.type = type
		self.quartal = quartal
		self.uri = uri
		self.timespan = decodeTimespan(self.year, self.quartal)

	def __repr__(self):
		return str(self.__dict__)


def downloadIter():
	"""Downloads and parser list of datasets"""
	resp = reqSess.get(dsListUri)
	resp.raise_for_status()
	doc = bs4.BeautifulSoup(resp.text, "html5lib")
	for l in list(map(lambda a: a["href"], doc.select("body > div > div > a"))):
		yield BackblazeDatasetDownload(l)


def datasetsListIntoTree(dList):
	"""Transforms list of datasets into a neested dict where keys are year and quartal"""
	d = {}
	for rec in dList:
		if rec.quartal:
			if not rec.year in d:
				d[rec.year] = [None] * 4
			d[rec.year][rec.quartal - 1] = rec
		else:
			d[rec.year] = rec
	return d


from plumbum import cli
from .DatabaseCommand import DatabaseCommand
from .NeedingOutputDirCommand import NeedingOutputDirCommand


class DatasetRetriever(DatabaseCommand):
	"""Creates a script to download the dataset from BackBlaze website using aria2c"""

	streamsCount = cli.SwitchAttr("--streamsCount", int, default=32, help="Max count of streams")
	incremental = cli.Flag("--incremental", default=None, help="Check db, download only the ones not in DB")
	destFolder = cli.SwitchAttr("--destFolder", cli.switches.MakeDirectory, default="./dataset", help="A dir to save dataset. Must be large enough.")

	def main(self):
		downloads = downloadIter()
		if self.incremental:
			with database.DBAnalyser() as db:
				lastDate = db.findLastDateTimeInAnalytics()
			print("the last date in the DB is " + str(lastDate), file=sys.stderr)
			downloads = [d for d in downloads if d.timespan[0] > lastDate]
			if not downloads:
				print("Good job, nothing to download, everything is in the base!", file=sys.stderr)
				return 0

			minDDate = min(downloads, key=lambda d: d.timespan[0]).timespan[0]
			maxDDate = max(downloads, key=lambda d: d.timespan[1]).timespan[1]
			print("The files to be downloaded will cover [" + str(minDDate) + ", " + str(maxDDate) + "] interval (" + str(maxDDate - minDDate) + ")", file=sys.stderr)

		print(__class__.genDownloadCommand((d.uri for d in downloads), self.destFolder, self.streamsCount))

	def genDownloadCommand(uris, destFolder, streamsCount=32, type="aria2"):
		streamsCount = str(streamsCount)
		if type == "aria2":
			return " ".join((commandGen.multilineEcho(uris), "|", "aria2c", "--continue=true", "--enable-mmap=true", "--optimize-concurrent-downloads=true", "-j", streamsCount, "-x 16", "-d", destFolder, "--input-file=-"))
		else:
			args = ["curl", "-C", "-", "--location", "--remote-name", "--remote-name-all", "--xattr"]
			args.extend(uris)
			return " ".join(args)


if __name__ == "__main__":
	DatasetRetriever.run()
